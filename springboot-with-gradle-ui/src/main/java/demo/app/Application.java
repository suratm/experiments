package demo.app;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Application {

    @RequestMapping("/app")
    @ResponseBody
    String home() {
        return "Hello World!";
    }

}
