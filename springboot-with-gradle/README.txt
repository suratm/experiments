Spring boot with gradle
Works as an executable war as well as a tomcat application

Compile : gradle clean build
run as spring boot :  gradle bootrun
Can run as a tomcat application - drop war file in tomcat webapp folder
log4j2, yaml configuration
